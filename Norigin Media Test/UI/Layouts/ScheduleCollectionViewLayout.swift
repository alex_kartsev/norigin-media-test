//
//  ScheduleCollectionViewLayoutAttributes.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/28/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import UIKit

protocol ScheduleCollectionViewLayoutDelegate: class {
    var sectionsCount: Int { get }
    var timeLineSupplementaryViewKind: String { get }
    var minutesFromZeroToNow: Int { get }
    func schedulesCount(for section: Int) -> Int
    func scheduleDuration(for indexPath: IndexPath) -> Int
}

class ScheduleCollectionViewLayout: UICollectionViewLayout {
    fileprivate let secondsInDay: CGFloat = 24 * 60
    fileprivate var contentHeight = 0
    fileprivate let contentWidth = 10000
    fileprivate let cellHeight = 100
    fileprivate let timeLineWidth = 2
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    var timeLineX: CGFloat = 0
    let leftBarWidth = 100
    weak var delegate: ScheduleCollectionViewLayoutDelegate?

    override func prepare() {
        guard let delegate = delegate else { return }
        var lastY = 0
        for section in 0 ..< delegate.sectionsCount {
            var lastX: CGFloat = CGFloat(leftBarWidth)
            for row in 0 ..< delegate.schedulesCount(for: section) {
                let attributesIndexPath = IndexPath(row: row, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: attributesIndexPath)
                let cellWidth: CGFloat = CGFloat(contentWidth * delegate.scheduleDuration(for: attributesIndexPath)) / secondsInDay
                attributes.frame = CGRect(x: Int(lastX), y: lastY, width: Int(cellWidth), height: cellHeight)
                lastX = lastX + cellWidth
                cache.append(attributes)
            }
            let footerAttr = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, with: IndexPath(item: 0, section: section))
            footerAttr.zIndex = 2
            footerAttr.frame = CGRect(x: Int(collectionView!.contentOffset.x), y: lastY, width: leftBarWidth, height: cellHeight)
            cache.append(footerAttr)
            lastY = lastY + cellHeight
        }
        contentHeight = lastY
        let timeLineAttr = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: delegate.timeLineSupplementaryViewKind, with: IndexPath(item: 0, section: 0))
        timeLineAttr.zIndex = 1
        timeLineX = CGFloat(contentWidth * delegate.minutesFromZeroToNow) / secondsInDay + CGFloat(leftBarWidth)
        timeLineAttr.frame = CGRect(x: Int(timeLineX), y: 0, width: timeLineWidth, height: contentHeight)
        cache.append(timeLineAttr)
    }

    override var collectionViewContentSize : CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache.first {
            $0.indexPath == indexPath
        }
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attributes in cache {
            if attributes.representedElementCategory == .supplementaryView, attributes.representedElementKind != delegate?.timeLineSupplementaryViewKind {
                attributes.frame = CGRect(x: collectionView!.contentOffset.x, y: attributes.frame.origin.y, width: attributes.frame.size.width, height: attributes.frame.size.height)
            }
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }

    override func invalidateLayout() {
        super.invalidateLayout()
        cache = []
    }
}

