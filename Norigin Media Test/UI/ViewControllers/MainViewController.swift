//
//  ViewController.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/25/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import UIKit
import ObjectMapper

class MainViewController: UIViewController {
    @IBOutlet weak var nowButton: RoundButton!
    @IBOutlet weak var collectionView: UICollectionView!
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    let viewModel = MainViewModel()
    private let cellReuseIdentifier = "ScheduleCell"
    private let lineSupplementaryKind = "LineSupplementaryKind"
    private let lineSupplementaryIdentifier = "LineSupplementaryIdentifier"

    private func showActivityIndicator() {
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
    }

    private func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    private func nowButton(hidden hide: Bool) {
        let alpha: CGFloat = hide ? 0.0 : 1.0
        if nowButton.alpha != alpha {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.nowButton.alpha = alpha
            }
        }
    }

    private func scrollToTimeLine(_ animated: Bool) {
        let layout = collectionView.collectionViewLayout as! ScheduleCollectionViewLayout
        let leftOffset = (collectionView.frame.size.width - CGFloat(layout.leftBarWidth))/2
        let scrollToX = layout.timeLineX - CGFloat(layout.leftBarWidth) - leftOffset
        collectionView.scrollRectToVisible(CGRect(x: scrollToX,
                                                  y: collectionView.contentOffset.y,
                                                  width: collectionView.frame.size.width,
                                                  height: collectionView.frame.size.height),
                                           animated: animated)
    }

    @IBAction func nowButtonPressed(_ sender: RoundButton) {
        scrollToTimeLine(true)
    }

    private func fetchData() {
        showActivityIndicator()
        viewModel.fetchData { error in
            hideActivityIndicator()
            if let error = error {
                showAlertMessage(title: "Fetching failed", message: error.localizedDescription)
            }
        }
    }
    private func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.black
        let layout = ScheduleCollectionViewLayout()
        layout.delegate = self
        collectionView.collectionViewLayout = layout
        collectionView.register(TimeLineReusableView.self, forSupplementaryViewOfKind: lineSupplementaryKind, withReuseIdentifier: lineSupplementaryIdentifier)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        fetchData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollToTimeLine(false)
    }
}

extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionsCount
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let channels = viewModel.arrayOfChannels {
            return channels[section].schedules.count
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath as IndexPath) as! ScheduleCell
        cell.viewModel = viewModel.viewModelForCell(indexPath)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterCollectionReusableView", for: indexPath) as! FooterCollectionReusableView
            view.logoImage.image = nil
            if let logoUrl = viewModel.urlToChannelLogo(for: indexPath.section) {
                view.logoImage.loadImageUsingCache(withUrl: logoUrl)
            }
            return view
        } else {
            let timeLine = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: lineSupplementaryIdentifier, for: indexPath)
            nowButton(hidden: true)
            return timeLine
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if view.reuseIdentifier == lineSupplementaryIdentifier {
            nowButton(hidden: false)
        }
    }
}

extension MainViewController: ScheduleCollectionViewLayoutDelegate {
    var sectionsCount: Int {
        get {
            if let channels = viewModel.arrayOfChannels {
                return channels.count
            }
            return 0
        }
    }

    var timeLineSupplementaryViewKind: String {
        get {
            return lineSupplementaryKind
        }
    }

    var minutesFromZeroToNow: Int {
        get {
            return viewModel.minutesFromZeroToFakeDate()
        }
    }

    func schedulesCount(for section: Int) -> Int {
        if let arrayOfChannels = viewModel.arrayOfChannels {
            return arrayOfChannels[section].schedules.count
        }
        return 0
    }

    func scheduleDuration(for indexPath: IndexPath) -> Int {
        return viewModel.scheduleDuration(for: indexPath)
    }
}

