//
//  HeaderReusableView.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/29/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import UIKit

class FooterCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var logoImage: UIImageView!
    private let backgroundBlackColor = UIColor(red: 45/255, green: 45/255, blue: 45/255, alpha: 1.0)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1
        backgroundColor = backgroundBlackColor
        layer.borderColor = UIColor.gray.cgColor
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 10, height: 0)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
        clipsToBounds = false
    }
}
