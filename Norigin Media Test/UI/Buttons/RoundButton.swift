//
//  OrangeRoundButton.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/29/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import UIKit

class RoundButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 5
    }
}
