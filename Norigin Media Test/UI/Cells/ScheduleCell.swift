//
//  ScheduleCell.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/28/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import UIKit

let defaultScheduleCellBackgroundColor = UIColor(red: 32/255,
                                                 green: 32/255,
                                                 blue: 32/255,
                                                 alpha: 1.0)

class ScheduleCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderWidth = 1
        layer.borderColor = UIColor.gray.cgColor
    }
    
    var viewModel: ScheduleCellViewModel! {
        didSet {
            titleLabel.text = viewModel.title
            timeLabel.text = viewModel.time
            if viewModel.shouldHighlighted {
                backgroundColor = UIColor.darkGray
            } else {
                backgroundColor = defaultScheduleCellBackgroundColor
            }
        }
    }
}
