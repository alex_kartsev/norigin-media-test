//
//  ScheduleCellViewModel.swift
//  Norigin Media Test
//
//  Created by Александр Карцев on 9/30/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import UIKit

class ScheduleCellViewModel {
    let title: String
    let time: String
    var shouldHighlighted: Bool
    
    init(title: String, time: String, shouldHighlighted: Bool) {
        self.title = title
        self.time = time
        self.shouldHighlighted = shouldHighlighted
    }
}
