//
//  MainViewModel.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/26/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation

class MainViewModel {
    fileprivate let fakeDateString = "2017-03-18 14:35"
    var fakeDate = Date()
    var fakeDateInNeededTimeZone = Date()
    let parseService = ParseService()
    let webService = WebService()
    let dateFormatter = DateFormatter()
    var channels: Channels?
    var arrayOfChannels: [Channel]? {
        get {
            return channels?.channels
        }
    }

    init() {
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        fakeDate = dateFormatter.date(from: fakeDateString)!
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 60*60)!
        fakeDateInNeededTimeZone = dateFormatter.date(from: fakeDateString)!
        dateFormatter.dateFormat = "HH:mm"
    }
    
    private func scheduleTime(for schedule: Schedule) -> String {
        return "\(dateFormatter.string(from: schedule.start)) - \(dateFormatter.string(from: schedule.end))"
    }
    
    private func shouldHighlighted(for schedule: Schedule) -> Bool {
        if fakeDateInNeededTimeZone >= schedule.start && fakeDateInNeededTimeZone <= schedule.end {
            return true
        }
        return false
    }

    func fetchData(completion: (_ error: Error?) -> Void) {
        webService.RESTDownloadData { data, error in
            if let error = error {
                completion(error)
            } else if let data = data {
                channels = parseService.parseChannels(data)
                completion(nil)
            }
        }
    }

    func scheduleDuration(for indexPath: IndexPath) -> Int {
        if let arrayOfChannels = arrayOfChannels {
            let schedule = arrayOfChannels[indexPath.section].schedules[indexPath.row]
            return schedule.durationInMinutes
        }
        return 0
    }

    func urlToChannelLogo(for section: Int) -> URL? {
        if let arrayOfChannels = arrayOfChannels {
            let channel = arrayOfChannels[section]
            if let imagesDict = channel.images, let logoUrlString = imagesDict["logo"] {
                return URL(string: logoUrlString)
            }
        }
        return nil
    }

    func minutesFromZeroToFakeDate() -> Int {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: fakeDate)
        let minutes = calendar.component(.minute, from: fakeDate)
        let dateInMinutes = hour * 60 + minutes
        return dateInMinutes
    }
    
    func viewModelForCell(_ indexPath:IndexPath) -> ScheduleCellViewModel {
        let schedule = arrayOfChannels![indexPath.section].schedules[indexPath.row]
        return ScheduleCellViewModel(title: schedule.title,
                                     time: scheduleTime(for: schedule),
                                     shouldHighlighted: shouldHighlighted(for: schedule))
    }
}
