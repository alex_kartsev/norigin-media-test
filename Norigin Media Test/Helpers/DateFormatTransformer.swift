//
//  DateFormatTransformer.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/25/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import ObjectMapper

final class DateFormatTransformer: DateFormatterTransform {
    static let dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    private static var formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.default
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = dateFormat
        return dateFormatter
    }()

    public init() {
        super.init(dateFormatter: DateFormatTransformer.formatter)
    }
}
