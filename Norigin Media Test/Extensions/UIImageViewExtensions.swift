//
//  UIImageViewExtensions.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/29/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import UIKit

fileprivate let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
    func loadImageUsingCache(withUrl url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        let stringUrl = url.absoluteString
        self.image = nil
        if let cachedImage = imageCache.object(forKey: stringUrl as NSString) as? UIImage {
            self.image = cachedImage
            return
        }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: stringUrl as NSString)
                    self.image = image
                }
            }
        }).resume()
    }
}
