//
//  UIViewControllerExtensions.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/26/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlertMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}
