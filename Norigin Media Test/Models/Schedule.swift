//
//  Schedule.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/25/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import ObjectMapper

struct Schedule {
    var id = ""
    var title = ""
    var start: Date = Date()
    var end: Date = Date()
    var durationInMinutes: Int {
        get {
            let components = Calendar.current.dateComponents([.minute], from: start, to: end)
            if let minutes = components.minute {
                return minutes
            }
            return 0
        }
    }
}

extension Schedule: Mappable {
    init?(map: Map) {
        mapping(map: map)
    }

    mutating func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        start <- (map["start"], DateFormatTransformer())
        end <- (map["end"], DateFormatTransformer())
    }
}
