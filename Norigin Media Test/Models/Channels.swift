//
//  Channels.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/25/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import ObjectMapper

struct Channels {
    var channels: [Channel] = []
}

extension Channels: Mappable {
    init?(map: Map) {
        mapping(map: map)
    }

    mutating func mapping(map: Map) {
        channels <- map["channels"]
    }
}
