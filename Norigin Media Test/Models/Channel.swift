//
//  Channel.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/25/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation
import ObjectMapper

struct Channel {
    var id = ""
    var title = ""
    var images: [String: String]?
    var schedules: [Schedule] = []
}

extension Channel: Mappable {
    init?(map: Map) {
        mapping(map: map)
    }

    mutating func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        images <- map["images"]
        schedules <- map["schedules"]
    }
}

