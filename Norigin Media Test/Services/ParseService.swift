//
//  ParseService.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/26/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation

final class ParseService {
    func parseChannels(_ data: Data) -> Channels? {
        if let string = String(data: data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
            return Channels(JSONString: string)
        } else {
            return nil
        }
    }
}
