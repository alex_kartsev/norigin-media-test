//
//  WebService.swift
//  Norigin Media Test
//
//  Created by Kartsau, Aliaksandr on 9/26/17.
//  Copyright © 2017 Kartsev, Alexander. All rights reserved.
//

import Foundation

final class WebService {
    private let localURL = "http://localhost:1337/epg"

    func RESTDownloadData(completion: (_ data: Data?, _ error: Error?) -> ()) {
        if let url = URL(string: localURL) {
            do {
                let data = try Data(contentsOf: url)
                completion(data, nil)
            } catch {
                completion(nil, error)
            }
        }
    }
}
